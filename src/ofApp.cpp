#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  ofBackground(0);
  ofSetFrameRate(60);
  font.load("Arial", 100, false, false, true);

  sentence = "Goldsmiths";
  stringPoints = getStringPoints(sentence);

  // YOUR CODE STARTS HERE
  
  noiseSeeds.resize(stringPoints.size());
  
  for (int i=0; i < stringPoints.size(); i++) {
    //see draew for description of the loop
    noiseSeeds[i] = (ofRandom(1000));
//    cout << i << ", " << noiseSeeds[i] << endl;       // test to see values
  }
  
  charmColor = ofColor(164,95,56);
  charmColor = ofColor(138,157,102);
  
  // YOUR CODE ENDS HERE
}

//--------------------------------------------------------------
void ofApp::update(){
  // YOUR CODE STARTS HERE


  // YOUR CODE ENDS HERE
}

//--------------------------------------------------------------
void ofApp::draw(){

  ofPushMatrix();
  ofTranslate(ofGetWidth()/2 - font.stringWidth(sentence)/2, ofGetHeight()/2 + font.stringHeight(sentence)/2);

  // YOU CODE STARTS HERE
  float noiseSize = ofMap(mouseX,0,ofGetWidth(),0,40);
  
  for (int i=0; i < stringPoints.size(); i++) {
    //loop conditions are (start point, continue condition, increment)
    //start point - new int variable called i, value is 0 - start from 0
    //continue condition - is i less than the number of entries in the stringPoints vector (stringPoints.size())
    //increment - how do we change i, in each loop - i++ - add 1 to i
    
    //create an offset which we will use to move each string point from it's existing position
    float x = ofSignedNoise(noiseSeeds[i]) * noiseSize;
    float y = ofSignedNoise(noiseSeeds[i] + 55) * noiseSize;
    ofPoint offset = ofPoint(x,y);
    
    //variable sized points
    float pointSize = ofSignedNoise(noiseSeeds[i]) * 6;
    
    ofPoint newPoint = stringPoints[i] + offset;
    
    
    ofSetColor(charmColor.getLerped(strangeColor,ofNoise(noiseSeeds[i] + 110)));
    
    ofDrawCircle(newPoint,pointSize);   //using drawCircle as ofPoint, radius function signature, ratehr than x,y,radius
    
    noiseSeeds[i] += 0.01;
  }
  
  
  // END OF YOUR CODE

  ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){

}

vector<ofPoint> ofApp::getStringPoints(string s){
  vector<ofPath> paths;
  vector<ofPoint> resultingPoints;
  string str = s;
  for(char& c : str) {
    paths.push_back(font.getCharacterAsPoints(c, true, false));
    paths[paths.size()-1].setMode(ofPath::POLYLINES);
  }

  ofPoint charOffset = ofPoint(0, 0);;
  for (int i = 0; i < paths.size(); i++){
     vector <ofPolyline> polylines = paths[i].getOutline();
     for (int j = 0; j < polylines.size(); j++){
        int numOfPoints = ofMap(polylines[j].getPerimeter(), 46, 574, 10, 100);
         for (int k=0; k<numOfPoints; k++){
             ofPoint letterPoint = ofPoint(polylines[j].getPointAtPercent(float(k+1)/numOfPoints));
             letterPoint += charOffset;
            resultingPoints.push_back(letterPoint);
        }
    }
    string letter = ofToString(str[i]);
    charOffset += ofPoint(font.stringWidth(letter), 0);
  }

  return resultingPoints;
}

//--------------------------------------------------------------
void ofApp::keyReleased  (int key){
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

